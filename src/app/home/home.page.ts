import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  weight: number;
  gender: string; 
  time: number; 
  bottle: number;
  promilles: number;
  //liters: number;
  //grams: number; 
  //burning: number;
  //gramsleft: number;
  

  constructor() {
  
  }
  calculate(){
    var liters= this.bottle * 0.33;
    var grams= liters * 8 * 4.5;
    var burning= this.weight / 10;
    var gramsleft= grams - (burning * this.time)

    if(this.gender === "m"){
      this.promilles = gramsleft / (this.weight * 0.7);

    } else {
      this.promilles = gramsleft / (this.weight * 0.6);
    }

    if (this.promilles<0){
      this.promilles=0;
    }
  }
}
